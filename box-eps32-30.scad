$fa=1;
$fs=0.1;

$height = 10;

module hole() {
    color("#ff0000") rotate([90,0,90]) cylinder($height + 1, 3/2, 3/2, true);
}


difference() {
    import_stl("box_bottom.stl");
    translate([5, 13, 12]) hole();
    translate([5, 17, 12]) hole();
    translate([5, 21, 12]) hole();
}