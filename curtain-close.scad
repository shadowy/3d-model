$fa=1;
$fs=0.1;

$height = 3;

module hole() {
    cylinder($height + 1, 3/2, 3/2, true);
    translate([0, 0, 2]) cylinder(3, 3/2+0.1, 5/2+0.1, true);
}

module socket() {
    cylinder($height + 1, 8/2+0.1, 8/2+0.1, true);  
}

module lip() {
    difference() {
        cube([60 ,50, $height], true);
        translate([50/2, 36/2, 0]) hole();
        translate([50/2, -36/2, 0]) hole();
        translate([-50/2, 36/2, 0]) hole();
        translate([-50/2, -36/2, 0]) hole();
        translate([0, 50/4, 0]) socket();
        translate([0, -36/2, 0]) hole();
    }
}

lip();